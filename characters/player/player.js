function Player() {
    //VARIABLES
    this.x = canvas.x/2;
    this.y = canvas.y/2;
    this.size = 30;
    this.speed = 2;
    this.health = 100;    
    this.kills;

    //USING this instead of a object so i can use the index to change the sprite being rendered in this.draw()
    this.normalSprites = [playerNoWeaponIMG, playerWithSwordIMG, playerWithShieldIMG, playerWithBowIMG, playerWithBombIMG];
    this.spriteIndex = 0;
    this.spriteArraySelector = "";
    this.swordAnimation = [playerSwordAnim0, playerSwordAnim1, playerSwordAnim2, playerSwordAnim3, playerSwordAnim4, playerSwordAnim5, playerSwordAnim6, playerSwordAnim7];
    this.bowAnimation = [playerBowAnim0, playerBowAnim1, playerBowAnim2, playerBowAnim3, playerBowAnim4, playerBowAnim5, playerBowAnim6, playerBowAnim7];
    this.bombAnimation = [playerBombAnim0, playerBombAnim1, playerBombAnim2, playerBombAnim3, playerBombAnim4, playerBombAnim5, playerBombAnim6, playerBombAnim7];
    this.animationTic = 37;

    this.weapons = [
                        {name: "Sword", hasPickedUp: false, ammoLeft: "∞"},
                        {name: "Shield", hasPickedUp: false, ammoLeft: "∞"},
                        {name: "Bow", hasPickedUp: false, ammoLeft: 0},
                        {name: "Bomb", hasPickedUp: false, ammoLeft: 0} 
                   ];
    

                //FLERA ARRAYS MED DE OLIKA ARRAYS I SIG SOM JAG BYTER MED OCH SEN KÖR JAG MED TIC +1 hela tiden om den inte redan är på 7

    this.swordAttackDamage = 70;

    this.shieldHealth;
    this.maxShieldHealth = 100;

    //The function that draws the character and animates!
    this.draw = () => {
        //CREATES A NEW TEMPORARY STYLE
        push();
        //SETS THE ORIGO TO THIS POSITION AND ROTATES TO FACE THE MOUSE
        translate(this.x, this.y);
        rotate(getRotation({x: this.x, y: this.y}, {x: mouseX, y: mouseY}));
        
        //CONTROLS THE ANIMATIONS
        //FI THE ANIMATIONTIC IS LESS THAN 8
        if(this.animationTic < 8) {
            //since the animations may have a different image size a temporary size is assigned with the normal img size by default
            let temporarySize = this.size;

            //SWITCHES THROUGH THE DIFFERENT ATTACKS
            switch(this.spriteArraySelector) {
                case "swordAttack":
                    //TEMP SIZE IS 80
                    temporarySize = 80;
                    //DRAWS IMAGE OF SWORD ANIMATION
                    image(this.swordAnimation[this.animationTic], -temporarySize/2, -temporarySize/2);
                    //ADDS ONE TO ANIMATIONTIC
                    this.animationTic++;
                    break;
                case "bowAttack":
                    //TEMP SIZE IS 60
                    temporarySize = 60;
                    //DRAWS IMAGE OF BOW ANIMATION
                    image(this.bowAnimation[this.animationTic], -temporarySize/2, -temporarySize/2);
                    //ADDS ONE TO THE ANIMATION TIC
                    this.animationTic++;
                    break;
                case "bombAttack":
                    //TEMP SIZE IS 60
                    temporarySize = 60;
                    //DRAWS IMAGE OF BOMB ANIMATION
                    image(this.bombAnimation[this.animationTic], -temporarySize/2, -temporarySize/2);
                    //ADDS ONE TO THE ANIMATION TIC
                    this.animationTic++;
                    break;
            }
        }
        //ELSE IF THE ANIMATION TIC IS 8 (END)
        else if(this.animationTic === 8) {
            //SWITCHES THROUGH THE DIFFERENT ATTACKS
            switch(this.spriteArraySelector) {
                case "swordAttack":
                    //RETURN TO THE NORMAL SWORD SPRITE
                    this.spriteIndex = 1;
                    break;
                case "bowAttack":
                    //ARROW WONT BE SHOT UNTILL LAST FRAME
                    //SHOOT ARROW
                    this.shootArrow();
                    //RETURNS TO THE STANDARD BOW SPRITE
                    this.spriteIndex = 3;
                    break;
                case "bombAttack":
                    //THROWS THE BOMB
                    this.throwBomb();
                    //IF THE LAST BOMB HAS BEEN THROWN THEN SWITCH IT TO THE EMPTY HANDED STARTER SPRITE
                    if(this.weapons[3].ammoLeft === 0) {
                        this.spriteIndex = 0;
                    } 
                    //ELSE SET IT TO THE NORMAL BOMB SPRITE
                    else {
                        this.spriteIndex = 4;
                    }
                    break;
            }
            //DRAW THE NORMAL SPRITE
            image(this.normalSprites[this.spriteIndex], -this.size/2, -this.size/2);
            //ADD ONE TO THE ANIMATION TIC
            this.animationTic++;
        }
        //ELSE DRAW A NORMAL SPRITE AND IF THE ANIMATION TIC IS LESS THAN 37 THEN ADD ONE TO IT
        else {
            image(this.normalSprites[this.spriteIndex], -this.size/2, -this.size/2);

            if(this.animationTic < 37) {
                this.animationTic++;
            } 

        }
        pop();
    }
// ------------------------------------------------------------------------------------------------------------------------------

// ITEM USAGE

    //CONTROLS THE SWITCHING OF WEAPONS
    this.switchWeapon = (index) => {
        //If not in animation and if i have picked up the weapon then switch
        if(!(this.animationTic < 7) && this.weapons[index-1].hasPickedUp === true) {
            
            //IF YOU SWITCH TO BOMB AND DONT HAVE ANY BOMBS YOU GET EMPTY HANDED STARTER SPRITE
            if(index === 4 && this.weapons[3].ammoLeft === 0) {
                this.spriteIndex = 0;
            }
            //ELSE GET THE SPRITE YOU SELECTED
            else {
                this.spriteIndex = index;
            }
        }
    }

    //CONTROLS THE USAGE OF ITEMS
    this.useItem = () => {
        //SWITCHES DEPENDING ON THE SPRITE INDEX
        switch(this.spriteIndex) {
            //IF CHARACTER IS EMPTY HANDED DO NOTHING
            case 0:
                break;
            //IF CHARACTER HOLDS A SWORD THEN DO SWORDATTACK
            case 1:
                this.swordAttack();
                break;
            //IF CHARACTER HOLDS A BOW THEN DO A BOW ATTACK
            case 3:
                this.bowAttack();
                break;
            //IF CHARACTER HOLDS A BOMB THEN DO A BOMB ATTACK
            case 4:
                this.bombAttack();
                break;
        }
    }

    //CONTROLS THE SWORD ATTACK
    this.swordAttack = () => {
        //SETS ANIMATION SPRITE SIZE TO 80
        let animSpriteSize = 80;
        
        //SETS THE SPRITE TO THE ANIMATION SPRITE
        
        //IF ANIMATION TIC IS 37
        //7 tics but the 30 extra creates a 0.5 second cooldown since it is playing in 60 fps
        if(this.animationTic === 37) {
            //SETS THE ANIMATION TIC TO 0 AND SETS THE SPRITE ARRAY SELECTOR TO SWORDATTACK
            this.animationTic = 0;
            this.spriteArraySelector = "swordAttack";


        //CHECK FOR ENEMIES COLLISIONS WITH THE ATTACK
        //LOOPS THROUGH THE ENEMIES
        for(let enemy in enemies) {
            for(let i = 0; i < enemies[enemy].length; i++) {
                //IF IT HITS A ENEMY THEN LET THEM TAKE DAMAGE
                if(collideCircleCircle(this.x, this.y, animSpriteSize, enemies[enemy][i].x, enemies[enemy][i].y, enemies[enemy][i].size)) {
                    enemies[enemy][i].takeDamage(this.swordAttackDamage);
                }
            }
        }
        }
    }

    //BOW ATTACK
    //THE ARRAY WILL BE SHOT IN THE LAST FRAME SO THE ANIMATION WILL WORK, CHECK DRAW FOR THE SHOOTARROW FUNCTION BEING USED
    this.bowAttack = () => {
        //IF THE ANIMATION TIC IS 37 AND YOU HAVE AMMO THEN SET THE ANIMATION TIC TO 0 AND SET SPRITE ARRAY SELECTOR TO BOW ATTACK
        if(this.animationTic === 37 && this.weapons[2].ammoLeft > 0) { 
            this.animationTic = 0;
            this.spriteArraySelector = "bowAttack";
        }
    }

    //SHOOT ARROW
    this.shootArrow = () => {
        //MAKES A NEW ARROW AND REMOVES ONE FROM ARROWS LEFT IN PLAYER INVENTORY
        arrowsInAir.push(new Arrow(createVector(this.x, this.y), createVector(mouseX, mouseY), "player"));
        this.weapons[2].ammoLeft -= 1;
    }

    //BOMB ATTACK
    this.bombAttack = () => {
        //IF THE ANIMATION TIC IS 37 AND YOU HAVE AMMO THEN SET THE ANIMATION TIC TO 0 AND SET SPRITE ARRAY SELECTOR TO BOMBATTACK
        if(this.animationTic === 37 && this.weapons[3].ammoLeft > 0) { 
            this.animationTic = 0;
            this.spriteArraySelector = "bombAttack";
        }
    }

    //THROWS BOMB
    this.throwBomb = () => {
        //MAKE A NEW BOMB AND REMOVE ONE BOMB FROM PLAYER INVENTORY
        bombsInAir.push(new Bomb(createVector(this.x, this.y), createVector(mouseX, mouseY), "player"));
        this.weapons[3].ammoLeft -= 1;
    }



// ------------------------------------------------------------------------------------------------------------------------------
    

//MOVEMENT HERE

    //THIS CONTROLS MOVEMENTS
    this.move = (movement) => {
        //x CONSTRAINTS

        //IF IT HASNT COLLIDED THEN MOVE
        //USED !this.checkWorldCollision() but the returned index 0 made the statement true
        if(this.checkWorldCollision(movement) === false) {

            //IF THE MOVEMENTS ARENT OUTSIDE OF THE BORDERS THEN ADD THE MOVEMENT TO X
            if(this.x + movement.x*this.speed < canvas.x &&
            this.x + movement.x*this.speed > 0) {
                this.x += movement.x*this.speed;
            } 
            //IF IT HITS A BORDER THEN SET THE POSITION TO THE EDGE
            else if (this.x + movement.x*this.speed > canvas.x) {
                this.x = canvas.x;
            }
            else if (this.x + movement.x*this.speed < 0) {
                this.x = 0;
            }

            //y CONSTRAINTS (200 is because of the topUI)
            //IF THE MOVEMENTS ARENT OUTSIDE OF THE BORDERS THEN ADD THE MOVEMENT TO Y
            if(this.y + movement.y*this.speed < canvas.y &&
            this.y + movement.y*this.speed > 200+this.size/2) {
                this.y += movement.y*this.speed;
            }
            //IF IT HITS A BORDER THEN SET THE POSITION TO THE EDGE
            else if (this.y + movement.y*this.speed > canvas.y) {
                this.y = canvas.y;
            }
            else if (this.y + movement.y*this.speed < 200+this.size/2) {
                this.y = 200+this.size/2;
            }

        }

        //CHECK IF MOVEMENT IS GOING AWAY FROM THE RAVINE OR TOWARDS
        else {
            //I is the index of the ravine that is collided with which is being returned by the function

            let i = this.checkWorldCollision(movement);
            //IF THE NEXT MOVEMENT ISNT GONNA COLLIDE WITH THE RAVINE THEN LET IT HAPPEN
            if(!(collideRectCircle(ravines[i].x, ravines[i].y, ravines[i].w, ravines[i].h, this.x + movement.x*this.speed, this.y + movement.y*this.speed, this.size))){
                this.x += movement.x*this.speed;
                this.y += movement.y*this.speed;
            }
        }

    }

    //CHECKS WORLD COLLISION
    this.checkWorldCollision = (movement) => {
        //3 ravines, I avoid referencing the variable since that uses more CPU
        for(i = 0; i < 3; i++) {
            //IF IT HITS A RAVINE
            if(collideRectCircle(ravines[i].x, ravines[i].y, ravines[i].w, ravines[i].h, this.x + movement.x*this.speed, this.y + movement.y*this.speed, this.size)) {
                //Returns the ravine index of the ravine hit
                return i;
            }
        }
        //IF NO COLLISIONS RETURN FALSE
        return false;
    }


// ------------------------------------------------------------------------------------------------------------------------------


// PICKING UP ITEMS AND WHAT HAPPENS AFTER

    //PICKS UP ITEMS
    this.pickUpItem = () => {
        //Check if there are any items to pickup
        if(droppedItems.length > 0) {
            //LOOPS THROUGH THE ITEMS
            for(let i = 0; i < droppedItems.length; i++) {
                //IF PLAYER HIT A DROPPED ITEM
                if(collideCircleCircle(droppedItems[i].x, droppedItems[i].y, droppedItemSize, this.x, this.y, this.size)) {
                    //SWITCHES BETWEEN THE DROPPED ITEM NAMES
                    switch(droppedItems[i].name) {
                        case "Sword":
                            //IF IT IS A SWORD THEN PICK IT UP
                            this.weapons[0].hasPickedUp = true;
                            break;
                        case "Shield":
                            //IF IT IS A SHIELD PICK IT UP AND SET IT TO MAX HEALTH
                            this.weapons[1].hasPickedUp = true;
                            this.shieldHealth = this.maxShieldHealth;
                            break;
                        case "Bow": 
                            //IF IT IS A BOW THEN PICK IT UP AND ADD SOME MORE ARROWS TO PLAYER INVENTORY
                            this.weapons[2].hasPickedUp = true;
                            this.weapons[2].ammoLeft += amountOfArrowsInDrop;
                            break;
                        case "Arrows":
                            //IF ARROWS THEN ADD ARROWS TO PLAYER INVENTORY
                            this.weapons[2].ammoLeft += amountOfArrowsInDrop;
                            break;
                        case "Bomb":
                            //IF BOMB THEN ADD PICK IT UP AND ADD IT TO PLAYER INVENTORY
                            this.weapons[3].hasPickedUp = true;
                            this.weapons[3].ammoLeft += 1;
                            break;
                    }
                    //Removes that item from the dropped items array
                    droppedItems.splice(i, 1);
                }
            }
        }
    }

    //TAKING DAMAGE

    //LETS PLAYER TAKE DAMAGE
    this.takeDamage = (damageTaken) => {
        //if shield is selected
        if(this.spriteIndex === 2) {
            //if the shield has less health than the damage that is taken then set shield hp to 0 and remove the rest from this.health, else just remove it from this.shieldhealth
            if(this.shieldHealth < damageTaken) {
                let leftOverDamageTaken = damageTaken - this.shieldHealth;
                //IF HEALTH IS LESS THEN THE LEFTOVERDAMAGETAKEN THEN GAME OVER
                if(leftOverDamageTaken > this.health) {
                    this.shieldHealth = 0;
                    this.health = 0;
                    game.over();
                }
                //IF IT ISNT MORE THAN HEALTH, JUST REMOVE LEFTOVERDAMAGETAKEN FROM HEALTH AND SET SHIELD TO 0
                else {
                    this.shieldHealth = 0;
                    this.health -= leftOverDamageTaken;
                }
            } 
            //IF THE SHIELDS HEALTH IS LARGER THAN THE DAMAGE TAKEN JUST REMOVE DAMAGE TAKEN FROM SHIELD HEALTH
            else {
                this.shieldHealth -= damageTaken;
            }
        }
        //IF SHIELD IS NOT SELECTED
        else {
            //IF HEALTH IS MORE THAN DAMAGE TAKEN JUST REMOVE DAMAGE TAKEN FROM HEALTH
            if(this.health - damageTaken > 0) {
                this.health -= damageTaken;
            }
            //IF YOU DIE THEN GAME OVER
            else {
                this.health = 0;
                game.over();
            }
        }
    }
    

}
