function Bomber(xSpawn, ySpawn) {
    //VARIABLES
    this.x = xSpawn;
    this.y = ySpawn;
    this.size = 30;
    this.speed = 3;
    this.range = 20;
    this.damage = 110;
    this.maxHealth = 50;
    this.health = this.maxHealth;

    //DRAWS THE BOMBER
    this.draw = () => {
        //CREATES A TEMPORARY STYLE 
        push();
        //SETS THE ORIGO TO THIS POSITION
        translate(this.x, this.y);
        //DRAWS THE IMAGE
        image(bomberStandardIMG, -this.size/2, -this.size/2);
        pop();

        //CREATES A NEW TEMPORARY STYLE
        push();
        translate(this.x, this.y);
        //BACKGROUND RECTANGLE
        rect(-this.size/2, -this.size+10, this.size, 5);
        
        //RED HEALTH BAR PART
        let healthBarLength = map(this.health, 0, this.maxHealth, 0, this.size)
        fill(201, 22, 22);
        //DRAWS THE RED RECTANGLE
        rect(-this.size/2, -this.size+10, healthBarLength, 5);
        pop();
    }

    //CONTROLS THE MOVEMENT
    this.move = () => {
        //CREATES VECTORS FOR THE TWO POINTS
        let playerVector = createVector(player.x, player.y);
        let bomberVector = createVector(this.x, this.y);

        //SUBTRACTS THE BOMBERVECTOR FROM PLAYERVECTOR
        playerVector.sub(bomberVector);
        //NORMALIZES PLAYERVECTOR
        playerVector.normalize();
        //MULTIPLIES PLAYERVECTOR BY SPEED
        playerVector.mult(this.speed);
        //ADDS PLAYERVECTOR TO BOMBERVECTOR
        bomberVector.add(playerVector);
        //SETS THIS POSITION TO BOMBERVECTOR
        this.x = bomberVector.x;
        this.y = bomberVector.y;
        
    }

    //CHECKS FOR ATTACK
    this.checkForAttack = (index) => {
        //IF THE DISTANCE IS LESS THAN RANGE
        if(dist(this.x, this.y, player.x, player.y) < this.range) {
            //BOMB ATTACKS
            this.bombAttack(index);
        }
    }

    //EXECUTES THE BOMB ATTACK
    //IT WILL SPLICE ITSELF IN THE SKETCH.JS
    this.bombAttack = (index) => {
        //PLAYER TAKES DAMAGE
        player.takeDamage(this.damage);
        //CREATES A NEW BOMB EXPLOSION
        bombExplosions.push(new BombExplosion(createVector(this.x, this.y)));
        //THIS TAKES DAMAGE AND DIES
        this.takeDamage(this.damage, index);
    }

    //CONTROLS DAMAGE TAKEN
    this.takeDamage = (damage, index) => {
        //IF IT DOESNT DIE THEN REMOVE DAMAGE FROM HEALTH
        if(this.health > damage) {
            this.health -= damage;
        }
        //IF IT DIES THEN ADD ONE TO THE PLAYER KILLS AND REMOVE ONE FROM ENEMIES LEFT AND REMOVE THE BOMBER
        else {
            player.kills += 1;
            game.enemiesLeft -= 1;
            enemies.bombers.splice(index, 1);
        }
    }

}