function SwordFighter(xSpawn, ySpawn) {
    //VARIABLES
    this.x = xSpawn;
    this.y = ySpawn;
    this.size = 40;
    this.maxHealth = 150;
    this.health = this.maxHealth;
    this.attackDamage = 30;
    this.attackRangeMultiplier = 1.5;
    this.attackTimer = 60;
    this.attackTimerMax = 60;
    this.speed = 1.5;

    //ANIMATIONS
    this.animationSize = 60;
    this.animationTic = 0;
    this.animationTicMax = 7;
    this.swordFighterAnimIMG = [swordFighterAnim0, swordFighterAnim1, swordFighterAnim2, swordFighterAnim3, swordFighterAnim4, swordFighterAnim5, swordFighterAnim6, swordFighterAnim7];

    this.draw = () => {
        
        push();
        translate(this.x, this.y);
        rotate(getRotation({x: this.x, y: this.y}, {x: player.x, y: player.y}));
        
        if(this.animationTic < this.animationTicMax) {
            image(this.swordFighterAnimIMG[this.animationTic], -this.animationSize/2, -this.animationSize/2);
            this.animationTic++;
        }
        else if(this.animationTic === this.animationTicMax) {
            image(this.swordFighterAnimIMG[this.animationTic], -this.animationSize/2, -this.animationSize/2);
            //this makes it one above so the statement under gets executed
            this.animationTic++;
        }
        else {
            image(swordFighterStandardIMG, -this.size/2, -this.size/2);
        }
        
        pop();
        push();
        translate(this.x, this.y);
        //BACKGROUND RECTANGLE
        rect(-this.size/2, -this.size+10, this.size, 5);
        
        //RED HEALTH BAR PART
        let healthBarLength = map(this.health, 0, this.maxHealth, 0, this.size)
        fill(201, 22, 22);
        rect(-this.size/2, -this.size+10, healthBarLength, 5);
        pop();
    }
    

    //A LITTLE UNCONVENTIONAL TO MAKE A VECTOR JUST TO UPDATE 1 X AND 1 Y VARIABLE BUT I PREFER HAVING XPOS AND YPOS SEPERATE.
    this.move = () => {

        let playerVector = createVector(player.x, player.y);
        let swordFighterVector = createVector(this.x, this.y);

        playerVector.sub(swordFighterVector);
        playerVector.normalize();
        playerVector.mult(this.speed);
        swordFighterVector.add(playerVector);
        this.x = swordFighterVector.x;
        this.y = swordFighterVector.y;

    }

    //I PASS THE INDEX SO I CAN DELETE THE EMEMY FROM THE ARRAY IN CASE IT DIES
    this.takeDamage = (damage, index) => {
        if(this.health > damage) {
            this.health -= damage;
        }
        else {
            player.kills += 1;
            game.enemiesLeft -= 1;
            enemies.swordFighters.splice(index, 1);
        }
    }

    //CHECKS FOR ATTACK
    this.checkForAttack = () => {
        //IF ATTACK COOLDOWN IS OVER
        if(this.attackTimer === this.attackTimerMax) {
            //IF ATTACK HITS 
            if(collideCircleCircle(this.x, this.y, this.size*this.attackRangeMultiplier, player.x, player.y, player.size)) {
                //ATTACK
                this.attack();
            }
            //RESET TIMER WHICH WILL LEAD TO A ATTACKTIMERMAX/60 (SECOND) COOLDOWN
            this.attackTimer = 0;
        }
    }
    
    //ATTACK
    this.attack = () => {
        //PLAYER TAKES DAMAGE
        player.takeDamage(this.attackDamage);
        //STARTS THE ANIMATION
        this.animationTic = 0;
    }

    //UPDATES THE ATTACK COOLDOWN
    this.updateAttackCooldown = () => {
        //IF THE TIMER IS LESS THEN TIMER MAX, UPDATE TIMER
        if(this.attackTimer < this.attackTimerMax) {
            this.attackTimer++;
        }
    }

}

