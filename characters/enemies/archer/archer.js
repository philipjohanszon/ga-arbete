function Archer(xSpawn, ySpawn) {
    //VARIABLES 
    this.x = xSpawn;
    this.y = ySpawn;
    this.maxHealth = 60;
    this.health = this.maxHealth;
    this.speed = 2.1;
    this.size = 30;
    this.range = 350;
    this.attackTimerMax = 120;
    this.attackTimer = 0;

    //ANIMATIONS
    this.animationSize = 60;
    this.animationTic = 0;
    this.animationTicMax = 7;
    this.archerAnimIMG = [archerAnim0, archerAnim1, archerAnim2, archerAnim3, archerAnim4, archerAnim5, archerAnim6, archerAnim7];
  

    //DRAWS THE ARCHER
    this.draw = () => {
        //CREATES A NEW TEMPORARY STYLE
        push();
        //SETS THE ORIGO TO TO THIS POSITION
        translate(this.x, this.y);
        //ROTATES IT TO FACE THE PLAYER
        rotate(getRotation({x: this.x, y: this.y}, {x: player.x, y: player.y}));
        
        //IF THE ANIMATION TIC IS LESS THAN THE MAX
        if(this.animationTic < this.animationTicMax) {
            //DRAW ONE OF THE ANIMATION FRAMES
            image(this.archerAnimIMG[this.animationTic], -this.animationSize/2, -this.animationSize/2);
            //ADDS ONE TO THE TIC
            this.animationTic++;
        }
        //ELSE IF THE ANIMATION TIC = THE ANIMATIONTICMAX
        else if(this.animationTic === this.animationTicMax) {
            //DRAWS THE LAST ANIMATION FRAME
            image(this.archerAnimIMG[this.animationTic], -this.animationSize/2, -this.animationSize/2);
            //SHOOTS ARROW IN LAST FRAME TO WORK WITH ANIMATION
            this.shootArrow();
            //ADDS ONE TO THE ANIMATION TIC
            this.animationTic++;
            //this makes it one above so the statement under gets executed
        }
        //ELSE DRAW THE NORMAL IMAGE
        else {
            image(archerStandardIMG, -this.size/2, -this.size/2);
        }
        //REMOVE THE STYLE
        pop();

        //CREATE A NEW TEMPORARY STYLE
        push();
        //SETS THE ORIGO TO TO THIS POSITION
        translate(this.x, this.y);
        //BACKGROUND RECTANGLE
        rect(-this.size/2, -this.size+10, this.size, 5);
        
        //RED HEALTH BAR PART
        let healthBarLength = map(this.health, 0, this.maxHealth, 0, this.size)
        fill(201, 22, 22);
        //RED HEALTH BAR PART OF THE RECTANGLE
        rect(-this.size/2, -this.size+10, healthBarLength, 5);
        pop();
    }

    //CONTROLS THE MOVEMENT
    this.move = () => {
        //IF THE ARCHER IS WITHIN THE DISTANCE {RANGE} OF THE PLAYER THEN DONT MOVE
        if(!(dist(this.x, this.y, player.x, player.y) < this.range)) {
            //CREATES VECTORS FOR BOTH POINTS
            let playerVector = createVector(player.x, player.y);
            let archerVector = createVector(this.x, this.y);

            //THIS SUBTRACTS ARCHER VECTOR FROM PLAYERVECTOR
            playerVector.sub(archerVector);
            //NORMALIZES IT
            playerVector.normalize();
            //MULTIPLIES IT BY THE SPEED
            playerVector.mult(this.speed);
            //ADDS PLAYERVECTOR TO ARCHER VECTOR
            archerVector.add(playerVector);
            //SETS THIS POSITION TO ARCHER VECTOR POSITION
            this.x = archerVector.x;
            this.y = archerVector.y;
        }

    }

    //CHECKS FOR ATTACK
    this.checkForAttack = () => {
        //IF THE DISTANCE IS WITHIN RANGE AND THE COOLDOWN IS OVER
        if(dist(this.x, this.y, player.x, player.y) < this.range && this.attackTimer === this.attackTimerMax) {
            //SETS THE ANIMATION TIC TO 0 AND THE ATTACK TIMER TO 0
            this.animationTic = 0;
            this.attackTimer = 0;
        }
    }
    
    //SHOOTS THE ARROW
    this.shootArrow = () => {
        //CREATES A NEW ARROW
        arrowsInAir.push(new Arrow(createVector(this.x, this.y), createVector(player.x, player.y), "enemy"));
    }

    //THIS UPDATES THE ATTACK COOLDOWN
    this.updateAttackCooldown = () => {
        //IF THE ATTACK COOLDOWN ISNT OVER THEN ADD ONE TO THE ATTACK TIMER
        if(this.attackTimer < this.attackTimerMax) {
            this.attackTimer++;
        }
    }

    //THIS LETS THE CHARACTER RECIEVE DAMAGE
    this.takeDamage = (damage, index) => {
        //IF THE HEALTH IS HIGHER THAN THE DAMAGE
        if(this.health > damage) {
            //SUBTRACT DAMAGE FROM HEALTH
            this.health -= damage;
        }
        else {
            //IF THE ARCHER DIES THEN RAISE PLAYER KILLS BY ONE AND LOWERS ENEMIES LEFT BY ONE AND REMOVE THE ARCHER
            player.kills += 1;
            game.enemiesLeft -= 1;
            enemies.archers.splice(index, 1);
        }
    }
}