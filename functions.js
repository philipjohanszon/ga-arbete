//ARGUMENTS ARE TWO POINTS
function getRotation(point1, point2) {
    //PUSH SO THAT TRANSLATE GETS REMOVED AND DOESNT RUIN ANYTHING ELSE
    push();
    translate(400, 600);
    //THE ANGLE IS ATAN2(Y - CENTER POINT.Y, X - CENTERPOINT.X)
    let angle = atan2(point2.y - point1.y, point2.x - point1.x);
    pop();
    //RETURNS ANGLE WITH A OFFSET
    return angle + 90; 
}

//DRAWS THE CURSOR
function drawCursor() {
    //PUSHES A NEW TEMPORARY STYLE
    push();
    //mouse - imgSize/2
    translate(mouseX-13.5, mouseY-13.5);
    //ROTATES THE IMAGE 45 DEGREES
    rotate(45);
    //DRAWS THE IMAGE
    image(cursorIMG, 0, 0, 27, 27);
    pop();
}


//SPAWNS ITEMS
function spawnItem(forcedItemName) {
    //IN CASE FORCED ITEM IS PROVIDED THEN ADD IT AS A ITEM NAME
    let itemName = forcedItemName;
    let itemX;
    let itemY;
    let itemSpawnIsOk = false;

    //IF NO ITEM NAME IS WRITTEN THEN SPAWN EITHER ARROWS OR A BOMB
    if(itemName === undefined) {
        let randomChance = int(random(0, 101));
        if(randomChance > bombDropChance) {
            itemName = "Arrows";
        } 
        else {
            itemName = "Bomb";
        }
    }

    //CREATE A RANDOM X AND Y POSITON
    while(!itemSpawnIsOk) {
        itemX = int(random(droppedItemSize, canvas.x - droppedItemSize));
        itemY = int(random(200 + droppedItemSize, canvas.y - droppedItemSize));
        //LOOPS THROUGH THE RAVINES AND CHECKS THAT THE ITEMS DONT COLLIDE WITH THE RAVINES SO WE DONT GET ITEMS THAT WE CANT GET
        for(let i = 0; i < ravines.length; i++) {
            if(collideRectCircle(ravines[i].x, ravines[i].y, ravines[i].w, ravines[i].h, itemX, itemY, droppedItemSize)) {
                break;
                
            }
            if(i === 2) {
                itemSpawnIsOk = true;
            }
        }

    }
    //PUSHES A NEW ITEM TO DROPPED ITEMS
    droppedItems.push({name: itemName, x: itemX, y: itemY});
}

//THIS DRAWS THE UI
function drawUI() {
    //THIS DRAWS THE TOP UI
    image(topUIIMG, 0, 0);

    //WRITING THE TEXT FOR HEALTH, SHIELD TEXT WILL BE IN THE SHIELD IF STATEMENT
    //CREATES NEW TEMPORARY STYLE
    push();
    //SETS TEXT FONT AND SIZE
    textFont(font);
    textSize(72);
    //RED COLOR THAT WRITES THE HEALTH
    fill(201, 22, 22);
    text(player.health, 120, 70);

      
    //ADDS A SWORD TO THE UI IF ITS PICKED UP
    if(player.weapons[0].hasPickedUp) {
        image(swordIMG, 130, 110, 60, 60);
    }

    //ADDS A SHIELD AND SHIELD HEALTH IF SHIELD IS PICKED UP
    if(player.weapons[1].hasPickedUp) {
        //ITEM IMAGE
        image(shieldIMG, 300, 110, 60, 60);
        //SHIELD AMOUNT UI TEXT IMAGE
        image(shieldUIIMG, 320, 20, 60, 60);
        fill(40, 188, 201);
        text(player.shieldHealth, 400, 70);
    }

    //ADDS A BOW TO THE UI IF ITS PICKED UP
    if(player.weapons[2].hasPickedUp) {
        image(bowIMG, 470, 110, 60, 60);
    }

    //ADDS A BOMB TO THE UI IF ITS PICKED UP
    if(player.weapons[3].hasPickedUp) {
        image(bombIMG, 640, 110, 60, 60);
    }

    //IF THE BOW IS SELECTED THEN DRAW A BOW IMG AND THE ARROWS LEFT IN THE UI
    if(player.spriteIndex === 3) {
        //AMOUNT OF ARROWS LEFT
        image(bowIMG, 620, 20, 60, 60)
        fill(0, 255, 0);
        text(player.weapons[2].ammoLeft, 700 ,70);
    }
    //IF THE BOMB IS SELECTED THEN DRAW A BOMB IMG AND THE AMOUNT OF BOMBS LEFT
    else if(player.spriteIndex === 4) {
        //AMOUNT OF BOMBS LEFT
        image(bombWithStrokeIMG, 620, 20, 60, 60)
        fill(0, 255, 0);
        text(player.weapons[3].ammoLeft, 700 ,70);
    }

    //REMOVE THE TEMPORARY STYLE
    pop();

}

//DRAWS THE DROPPED ITEMS
function drawDroppedItems() {
    //LOOPS THROUGH ALL DROPPEDITEMS
    for(let i = 0; i < droppedItems.length; i++) {
        
        //DRAWS A HALO AROUND THE DROPPED ITEM
        push();
        fill(255, 255, 0, 70);
        noStroke();
        ellipse(droppedItems[i].x, droppedItems[i].y, droppedItemSize*1.5, droppedItemSize*1.5);
        pop();
        
        //DEPENDING ON THE ITEM, DRAW A IMAGE OF IT IN THE POSITION THAT ITS LOCATED
        switch(droppedItems[i].name) {
            case "Sword":
                image(swordIMG, droppedItems[i].x - droppedItemSize/2, droppedItems[i].y - droppedItemSize/2, droppedItemSize, droppedItemSize);
                break;
            case "Shield":
                image(shieldIMG, droppedItems[i].x - droppedItemSize/2, droppedItems[i].y - droppedItemSize/2, droppedItemSize, droppedItemSize);
                break;
            case "Bow":
                image(bowIMG, droppedItems[i].x - droppedItemSize/2, droppedItems[i].y - droppedItemSize/2, droppedItemSize, droppedItemSize);
                break;
            case "Arrows":
                image(bowIMG, droppedItems[i].x - droppedItemSize/2, droppedItems[i].y - droppedItemSize/2, droppedItemSize, droppedItemSize);
                break; 
            case "Bomb":
                image(bombIMG, droppedItems[i].x - droppedItemSize/2, droppedItems[i].y - droppedItemSize/2, droppedItemSize, droppedItemSize);
                break;
        }
        
    }
}

//RETRIEVES THE DIRECTION OF TRAVEL
function directionOfTravel(x1, y1, x2, y2) {
    //SET A VECTOR FOR THE TWO POINTS
    let target = createVector(x2, y2);
    let arrowPosition = createVector(x1, y1);

    //THE DISTANCE IS THE DISTANCE BETWEEN THE TWO
    let distance = target.dist(arrowPosition);
    //MAPS THE DISTANCE
    let mappedDistance = map(distance, 0, 100, 1, 0);

    //SUBTRACTS THE ARROWPOSITION
    target.sub(arrowPosition);
    //NORMALIZE THE TARGET
    target.normalize();

    //RETURN THE DIRECTION
    return target;

}

