function Game() {
    //ROUND VARIABLES
    this.notStarted = true;
    this.currentRound = 0;
    this.roundCooldown = 500;
    this.roundTic = 0;

    //RESET BUTTON
    this.resetButton;

    //SPAWNS
    this.swordFightersLeft = 0;
    this.archersLeft = 0;
    this.bombersLeft = 0;
    this.spawnCooldown = 100;
    this.spawnTic = this.spawnCooldown;

    //This is for calculating spawning of enemies every round
    this.enemySpawnScore = 0;
    this.maxEnemiesOnScreen = 10;
    this.enemiesLeft = 0;

    //SPAWNSCORES
    this.archerSpawnScore = 350;
    this.bomberSpawnScore = 550;
    this.swordFighterSpawnScore = 100;


    //THIS IS RUN IN THE SETUP AND WILL START THE GAME
    this.begin = () => {

        //MAKE NEW TEXT THAT SAYS WELCOME AND PICK UP THE ITEMS TO BEGIN
        UITexts.push(new UIText("Welcome",createVector(400, 400), 72, 100));
        UITexts.push(new UIText("Pick up the items to begin",createVector(400, 500), 48, 200));
        //SPAWN A SWORD AND A SHIELD AND SET THE ROUND TO 0
        spawnItem("Sword");
        spawnItem("Shield");
        this.currentRound = 0;

    }


    //THIS IS RUN WHEN ALL THE ENEMIES ARE DEAD AND WILL START A NEW ROUND
    this.startNewRound = () => {
        //ADD ONE TO THE CURRENT ROUND
        this.currentRound += 1;
        //SET THE SPAWN SCORE
        this.enemySpawnScore = 250*this.currentRound;
        //CALCULATE THE SPAWNINGS
        this.calculateEnemySpawnings();
    }

    //CHECK IF ALL THE ENEMIES ARE DEAD AND START A NEW ROUND IF THEY ARE
    this.checkForNewRound = () => {
        //IF ALL ENEMIES ARE DEAD AND THE ROUND IS ABOVE 0 START A GAME (IF THE LAST PART WASNT ADDED THEN IT WOULD START THE GAME BEFORE YOU PICK UP THE SWORD AND SHIELD)
        if(this.enemiesLeft === 0 && this.currentRound > 0) {
            //IF THE TIME BETWEEN ROUNDS IS OVER THEN START A NEW ROUND AND RESET THE COOLDOWN
            if(this.roundTic === this.roundCooldown) {
                this.startNewRound();
                this.roundTic = 0;
            }
            //ELSE IF THE ROUNDTIC IS 0 AND THE ROUND IS HIGHER THAN 0 THEN SPAWN ITEMS, WRITE A TEXT THAT DISPLAYS THE ROUND AND ALSO ADD 1 TO THIS.ROUNDTIC SO THIS ISNT RUN AGAIN BEFORE THE NEXT ROUND
            else if(this.roundTic === 0 && this.currentRound > 0) {
                this.spawnItems();
                //ADDS +1 to this round otherwise it will show the last round
                UITexts.push(new UIText("Round: " + String(this.currentRound+1), createVector(400, 400), 72, 500));
                this.roundTic++;
            }
            //ELSE IF THE ROUNDTIC IS ABOVE 0 AND THE ROUND TIC IS LESS THAN THE ROUND COOLDOWN AND THERE ARE NO ENEMIES LEFT (OTHERWISE IT WOULD INCREASE TIC DURING THE ROUND) THEN IT WILL INCREASE TIC
            else if(this.roundTic > 0 && this.roundTic < this.roundCooldown && this.enemiesLeft === 0) {
                this.roundTic++;
            }
            
        }
    }

    //THIS SPAWNS THE ENEMIES DEPENDING ON WHAT ENEMIESSPAWNS ARE LEFT
    this.spawnEnemies = () => {
        //IF THE SPAWN COOLDOWN IS NOT OVER THEN INCREASE SPAWNTIC
        if(this.spawnTic < this.spawnCooldown) {
            this.spawnTic++;
        }
        //ELSE IF THE COOLDOWN IS OVER AND ALL THE ENEMIES IN THE GAME CURRENTLY ARE LESS THAN 10 (10 MAX)
        //IT CAN GO UP TO MAX ENEMIES ON SCREEN BUT NOT OVER
        else if(enemies.swordFighters.length + enemies.archers.length + enemies.bombers.length < this.maxEnemiesOnScreen) {

            //CHOSE A RANDOM RAVINE TO SPAWN IN AND CHOSE THE X AND Y SPAWN RANDOMLY FROM THE RAVINES X AND Y POSITIONS
            let ravine = int(random(0, 3));
            let xSpawn = ravines[ravine].x + random(0, ravines[ravine].w);
            let ySpawn = ravines[ravine].y + random(0, ravines[ravine].h);

            //RANDOMISE THE ORDER OF SPAWNING IF THERE IS ONE OF EVERYONE LEFT TO SPAWN
            if(this.swordFightersLeft > 0 && this.archersLeft > 0 && this.bombersLeft > 0) {
                    //CREATE A RANDOM NUMBER BETWEEN 0 AND 3
                    let randomNum = int(random(0, 3));
                    //IF THE RANDOM NUMBER IS 0 THEN SPAWN A ARCHER, IF 1 SPAWN A SWORDFIGHTER, IF 2 SPAWN A BOMBER
                    if(randomNum === 0) {
                        //MAKE A NEW ARCHER
                        enemies.archers.push(new Archer(xSpawn, ySpawn));
                        //LOWER THE AMOUNT OF ARCHERS LEFT THAT WAS DECIDED BY CALCULATEENEMYSPAWNINGS
                        this.archersLeft -= 1;
                        //SET THE SPAWNTIC TO 0 WHICH RESETS THE SPAWN COOLDOWN
                        this.spawnTic = 0;
                    }
                    else if(randomNum === 1) {
                        //MAKE A NEW SWORDFIGHTER
                        enemies.swordFighters.push(new SwordFighter(xSpawn, ySpawn));
                        //LOWER THE AMOUNT OF SWORDFIGHTERS LEFT THAT WAS DECIDED BY CALCULATEENEMYSPAWNINGS
                        this.swordFightersLeft -= 1;
                        //SET THE SPAWNTIC TO 0 WHICH RESETS THE SPAWN COOLDOWN
                        this.spawnTic = 0;
                    }
                    else if(randomNum === 3 ){
                        //MAKE A NEW BOMBER
                        enemies.bombers.push(new Bomber(xSpawn,ySpawn));
                        //LOWER THE AMOUNT OF BOMBERS LEFT THAT WAS DECIDED BY CALCULATEENEMYSPAWNINGS
                        this.bombersLeft -= 1;
                        //SET THE SPAWNTIC TO 0 WHICH RESETS THE SPAWN COOLDOWN
                        this.spawnTic = 0;
                    }
            }
            //ELSE JUST SPAWN THE ONES THAT ARE LEFT AND ARE POSSIBLE TO SPAWN
            else {
                //IF THERE ARE ARCHERS LEFT AND ALSO SPACE LEFT 
                if(this.archersLeft > 0 && enemies.swordFighters.length + enemies.archers.length + enemies.bombers.length < this.maxEnemiesOnScreen) {
                    //MAKE A NEW ARCHER
                    enemies.archers.push(new Archer(xSpawn, ySpawn));
                    //LOWER THE AMOUNT OF ARCHERS LEFT THAT WAS DECIDED BY CALCULATEENEMYSPAWNINGS
                    this.archersLeft -= 1;
                    //SET THE SPAWNTIC TO 0 WHICH RESETS THE SPAWN COOLDOWN
                    this.spawnTic = 0;
                }
                //IF THERE ARE SWORDFIGHTERS LEFT AND ALSO SPACE LEFT
                else if(this.swordFightersLeft > 0 && enemies.swordFighters.length + enemies.archers.length + enemies.bombers.length < this.maxEnemiesOnScreen) {
                    //MAKE A NEW SWORDFIGHTER
                    enemies.swordFighters.push(new SwordFighter(xSpawn, ySpawn));
                    //LOWER THE AMOUNT OF SWORDFIGHTERS LEFT THAT WAS DECIDED BY CALCULATEENEMYSPAWNINGS
                    this.swordFightersLeft -= 1;
                    //SET THE SPAWNTIC TO 0 WHICH RESETS THE SPAWN COOLDOWN
                    this.spawnTic = 0;
                }
                //IF THERE ARE BOMBERS LEFT AND ALSO SPACE LEFT
                else if(this.bombersLeft > 0 && enemies.swordFighters.length + enemies.archers.length + enemies.bombers.length < this.maxEnemiesOnScreen) {
                    //MAKE A NEW BOMBER
                    enemies.bombers.push(new Bomber(xSpawn,ySpawn));   
                    //LOWER THE AMOUNT OF BOMBERS LEFT THAT WAS DECIDED BY CALCULATEENEMYSPAWNINGS
                    this.bombersLeft -= 1;
                    //SET THE SPAWNTIC TO 0 WHICH RESETS THE SPAWN COOLDOWN
                    this.spawnTic = 0;
                }
            }
        }
        
    }

    //Calculates enemy spawnings based on this.enemyspawnscore
    this.calculateEnemySpawnings = () => {
        //VARIABLES FOR THE AMOUNT SPAWNING
        let swordFightersSpawning = 0;
        let archerSpawning = 0;
        let bomberSpawning = 0;
        //JUST A LIMIT FOR A IF STATEMENT
        let swordfighterSpawningLimit = 3;
        
            //LOOPS THROUGH AND MAKES THE SPAWNINGS OF MAX 10 ENEMIES
            for(let i = 0; i < this.maxEnemiesOnScreen; i++) {
                //SINCE AT HIGHER SPAWNSCORES THE SWORDFIGHTERS GET RARE ILL MAKE 3 SWORDFIGHTERS IF IT ALLOWS FOR IT
                if(this.enemySpawnScore > 999 && swordfighterSpawningLimit > 0) {
                    //ADD ONE TO THE VARIABLE
                    swordFightersSpawning++;
                    //REMOVE ONE FROM THE LIMIT
                    swordfighterSpawningLimit -= 1;
                    //REMOVE THIS AMOUNT FROM THE SPAWNSCORE
                    this.enemySpawnScore -= this.swordFighterSpawnScore;
                }
                //ELSE IF THE SCORE ALLOWS FOR A BOMBER THEN SPAWN ONE
                else if(this.enemySpawnScore - this.bomberSpawnScore > 0){
                    //ADD ONE TO THE VARIABLE
                    bomberSpawning++;
                    //REMOVE THIS AMOUNT FROM THE SPAWNSCORE
                    this.enemySpawnScore -= this.bomberSpawnScore;
                }
                //ELSE IF THE SCORE ALLOWS FOR A ARCHER THEN SPAWN ONE
                else if(this.enemySpawnScore - this.archerSpawnScore > 0) {
                    //ADD ONE TO THE VARIABLE
                    archerSpawning++;
                    //REMOVE THIS AMOUNT FROM THE SPAWNSCORE
                    this.enemySpawnScore -= this.archerSpawnScore;
                }
                //ELSE IF THE SCORE ALLOWS FOR A SWORDFIGHTER THEN SPAWN ONE
                else if(this.enemySpawnScore - this.swordFighterSpawnScore > 0) {
                    //ADD ONE TO THE VARIABLE
                    swordFightersSpawning++;
                    //REMOVE THIS AMOUNT FROM THE SPAWNSCORE
                    this.enemySpawnScore -= this.swordFighterSpawnScore;
                }   
            }

        //SUM UP THE AMOUNT OF ENEMIES THAT WILL SPAWN
        this.enemiesLeft = archerSpawning + swordFightersSpawning + bomberSpawning;
        //SET THE AMOUNT OF EACH ENEMY THAT WILL SPAWN
        this.archersLeft = archerSpawning;
        this.bombersLeft = bomberSpawning;
        this.swordFightersLeft = swordFightersSpawning;
    }

    //THIS FUNCTION SPAWNS ITEMS
    this.spawnItems = () => {
        //IF THE ROUND IS 0 IT WILL NOT SPAWN ANYTHING SINCE THE BEGIN FUNCTION DOES IT
        if(this.currentRound === 0) {

        }
        //ELSE IF THE ROUND IS 1 THEN SPAWN A BOW (IT IS GOING TO BE ROUND 2)
        else if(this.currentRound === 1) {
            spawnItem("Bow");
        }
        //ELSE IF THE ROUND IS 2 THEN SPAWN A BOMB ARROWS AND A SHIELD (IT IS GOING TO BE ROUND 3)
        else if(this.currentRound === 2) {
            spawnItem("Bomb");
            spawnItem("Arrows");
            spawnItem("Shield");
        }
        //EVERY THIRD + 1 ROUND WILL HAVE 2 SPAWNS AND A SHIELD SPAWN
        else if(this.currentRound % 3 === 0 && this.currentRound > 3) {
            spawnItem();
            spawnItem();
            spawnItem("Shield");
        }
        //IF NOTHING ELSE APPLIES, SPAWN A RANDOM ITEM
        else {
            spawnItem();
        }
    }

    //IF THE GAME IS OVER THEN THIS WILL RUN
    this.over = () => {
        //THIS STOPS THE MAIN GAME LOOP
        noLoop();

        //DRAWS A GREY BACKGROUND
        background(31, 31, 31);

        //MAKES A NEW TEMPORARY STYLE AND WRITES OUT TEXT ON THE END SCREEN
        push();
        //FILL WHITE
        fill(255, 255, 255);
        textSize(72);
        //ALIGN THE TEXT FROM THE CENTER
        textAlign(CENTER, CENTER);
        //WRITE YOU DIED, X = 400, Y = 300
        text("YOU DIED", 400, 300);
        textSize(60);
        //WRITE YOU SURVIVED UNTIL ROUND:, x = 400, Y = 800
        text("You survived until round:", 400, 800);
        textSize(80);
        //WRITE THE ROUND YOU DIED ON, X = 400, Y = 900
        text(this.currentRound, 400, 900);
        //CREATE A BUTTON
        this.createButton();
        pop();
    
        //SET PLAYER TO UNDEFINED. IT WILL BE REDEFINED IN SETUP
        player = undefined;
        //REMOVE ALL ENEMIES
        enemies.swordFighters = [];
        enemies.archers = [];
        enemies.bombers = [];
        //REMOVE DROPPED ITEMS AND ALSO PROJECTILES
        droppedItems = [];
        bombsInAir = [];
        arrowsInAir = [];
        //SET THE ENEMIES LEFT TO 0 AND RESET SPAWNCOOLDOWN AND ALSO SET NOTSTARTED TO TRUE
        this.enemiesLeft = 0;
        this.spawnTic = this.spawnCooldown;
        this.notStarted = true;
    }

    //THIS CREATES A BUTTON THAT SAYS PRESS ME TO RESTART AND ALSO HAS SOME STYLES TO IT. IF PRESSED IT WILL EXECUTE THIS.RESET()
    this.createButton = () => {
        this.resetButton = createButton("PRESS ME TO RESTART");
        this.resetButton.mousePressed(this.reset);
        this.resetButton.position(300, 500);
        this.resetButton.style("width", "200px");
        this.resetButton.style("height", "100px");
        this.resetButton.style("font-size", "20pt")
        this.resetButton.style("font-weight", "bold");
        this.resetButton.style("color", "#4f4f4f");
        this.resetButton.style("background-color", "white")
    }


    //RESETS THE GAME, IT WILL REMOVE THE RESETBUTTON, START SETUP AND LOOP
    this.reset = () => {
        this.resetButton.hide();
        setup();
        loop();
    }

}