function Arrow(pos, targetPosition, arrowShooter) {
    //VARIABLES
    this.direction = directionOfTravel(pos.x, pos.y, targetPosition.x, targetPosition.y);
    this.length = 20;
    //USE START DISTANCE OTHERWISE THE ARROW END WILL BE BEHIND THE PLAYER
    this.startDistance = 30;
    //POSITION FOR THE FRONT AND END OF THE ARROW
    this.position = createVector(pos.x + this.direction.x*this.startDistance, pos.y + this.direction.y*this.startDistance);
    this.endPosition = createVector(this.position.x - this.direction.x*this.length, this.position.y - this.direction.y*this.length);
    this.damage = 120;
    this.damageMultiplier = 1;
    this.speed = 10;
    this.speedMultiplier = 1;
    this.shooter = arrowShooter;
    this.getRemoved = false;
    
    //IF IT IS A ENEMY THEN SET SOME PENALTIES ON THE ARROW DAMAGE AND SPEED
    if(arrowShooter === "enemy") {
        this.damageMultiplier = 0.5;
        this.speedMultiplier = 0.8;
    }

    //THIS DRAWS THE ARROW
    this.draw = () => {
        //PUSH A NEW STYLE
        push();
        //FILL WITH THE COLOR OF THE ARROW
        fill(242, 190, 121);
        strokeWeight(2);
        stroke(242, 190, 121);
        //DRAWS THE SHAFT OF THE ARROW
        line(this.position.x, this.position.y, this.endPosition.x, this.endPosition.y);
        //FILL WITH THE TIP OF THE ARROW
        fill(31, 31, 31);
        strokeWeight(0.5);
        stroke(0,0,0);
        //DRAWS THE TIP OF THE ARROW
        ellipse(this.position.x, this.position.y, 5);
        pop();
    }

    //THIS CONTROLS THE ARROWS MOVEMENT
    this.move = () => {
        //IF IT IS OUTSIDE OF THE MAP THEN REMOVE IT
        if(this.position.x > 800+this.length || this.position.x < 0-this.length) {
            this.getRemoved = true;
        }
        //IF IT IS OUTSIDE OF THE MAP THEN REMOVE IT
        if(this.position.y > 1000+this.length || this.position.y < 200-this.length) {
            this.getRemoved = true;
        }
        //IF IT IS INSIDE THE MAP THEN GET THE POSITION BY ADDING THE DIRECTION MULTIPLIED BY SPEED AND SPEEDMULTIPLIER
        this.position.x += this.direction.x*this.speed*this.speedMultiplier;
        this.position.y += this.direction.y*this.speed*this.speedMultiplier;
        //GET THE POSITION OF THE BACKSIDE OF THE ARROW BY SUBTRACTING THE DIRECTION MULTIPLIED BY THE LENGTH FROM POSITION
        this.endPosition.x = this.position.x - this.direction.x*this.length;
        this.endPosition.y = this.position.y - this.direction.y*this.length;
    }

    //IF IT HITS A CHARACTER
    this.hitCharacter = () => {
        //IF A PLAYER SHOT THE ARROW
        if(arrowShooter === "player") {
            //LOOP THROUGH ALL ENEMIES
            for(let enemy in enemies) {
                for(let i = 0; i < enemies[enemy].length; i++) {
                    //IF IT COLLIDES WITH A ENEMY
                    if(collideLineCircle(this.position.x, this.position.y, this.endPosition.x, this.endPosition.y, enemies[enemy][i].x, enemies[enemy][i].y, enemies[enemy][i].size)) {
                        //THE ARROW WILL BE REMOVED AND THE ENEMY WILL TAKE DAMAGE
                        this.getRemoved = true;
                        enemies[enemy][i].takeDamage(this.damage, i);
                    }
                }
            }
        }
        //IF A ENEMY SHOT THE ARROW 
        else if(arrowShooter === "enemy") {
            //IF IT THE PLAYER
            if(collideLineCircle(this.position.x, this.position.y, this.endPosition.x, this.endPosition.y, player.x, player.y, player.size)) {
                //THE ARROW WILL BE REMOVED AND THE PLAYER WILL TAKE DAMAGE
                this.getRemoved = true;
                player.takeDamage(this.damage*this.damageMultiplier, i);
            }
        }
    }


    //SINCE I HAD A BUG WHERE HIT CHARACTER WOULD NOT WORK IF IT ALREADY GOT REMOVED FROM THIS.MOVE I WILL REMOVE IT IN A SEPERATE FUNCTION
   
    //THIS JUST RETURNS TRUE IF GET REMOVED IS TRUE AND THEN WITH A FOR LOOP IN SKETCH IT IS REMOVED
    this.selfDestruct = () => {
        if(this.getRemoved) {
            return true;
        }
    }

}