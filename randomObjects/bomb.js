function Bomb(pos, targetPosition, bombThrower) {
    //VARIABLES
    this.direction = directionOfTravel(pos.x, pos.y, targetPosition.x, targetPosition.y);
    this.targetPosition = createVector(targetPosition.x, targetPosition.y);
    this.startingDistance = 20;
    this.position = createVector(pos.x + this.direction.x*this.startingDistance, pos.y + this.direction.y*this.startingDistance);
    this.speed = 4;
    this.damage = 200;
    this.size = 10;
    this.explosionSize = 5;


    //DRAWS THE BOMB
    this.draw = () => {
        //CREATES A NEW TEMPORARY STYLE
        push();
        //REMOVE STROKE
        noStroke();
        //SET THE ORIGO TO THE BOMBS POSITION
        translate(this.position.x, this.position.y);
        //Rotate because the bomb image is rotated in the wrong direction
        rotate(270);
        //DRAW THE IMAGE
        image(bombIMG, 0, 0, this.size, this.size);
        pop();
    }
    
    //THIS CONTROLS THE MOVEMENT
    this.move = () => {
        //THE POSITION IS CALCULATED BY ADDING DIRECTION * SPEED TO THE POSITION
        this.position.x += this.direction.x*this.speed;
        this.position.y += this.direction.y*this.speed;
    }

    //CHECK IF THE BOMB HAS LANDED
    this.checkIfLanded = () => {
        //IF THE BOMB IS WITHIN 20 PIXELS OF THE BOMB TARGET POSITION
        if(dist(this.position.x, this.position.y, this.targetPosition.x, this.targetPosition.y) < 20) {
            //SET THE SPEED TO 0
            this.speed = 0;
            //EXECUTE HITCHARACTERS
            this.hitCharacter();
            //MAKE A NEW BOMB EXPLOSION
            bombExplosions.push(new BombExplosion(this.position));
            //RETURN TRUE SO IT CAN BE DELETED IN SKETCH
            return true;
        }
    }

    //CHECK IF IT HIT A ENEMY
    this.hitCharacter = () => {
        //IF THE BOMB THROWER IS A PLAYER
        if(bombThrower === "player") {
            //LOOP THROUGH THE ENEMIES
            for(let enemy in enemies) {
                for(let i = 0; i < enemies[enemy].length; i++) {
                    //IF IT HIT A ENEMY
                    if(collideCircleCircle(this.position.x, this.position.y, this.size*this.explosionSize, enemies[enemy][i].x, enemies[enemy][i].y, enemies[enemy][i].size)) {
                        //THE ENEMY TAKES DAMAGE
                        enemies[enemy][i].takeDamage(this.damage, i);
                    }
                }
            }
        }
    }
    
 }