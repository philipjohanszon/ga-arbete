function UIText (string, pos, size, duration) {
    //VARIABLE
    this.currentDurationFrame = 0;

    //DRAWS THE UI TEXT
    this.draw = () => {
        //PUSH A NEW TEMPORARY STYLE
        push();
        //DRAWS THE TEXT IN WHITE
        fill(255, 255, 255);
        textSize(size);
        //ALIGNS THE TEXT CENTER TOP
        textAlign(CENTER, TOP);
        //DRAWS THE TEXT
        text(string, pos.x, pos.y);
        pop();
        //ADDS ONE TO THE CURRENT DURATION 
        this.currentDurationFrame++;
    }

    //IF THE DURATION HAS PASSED THEN REMOVE IT IN SKETCH
    this.delete = () => {
        if(this.currentDurationFrame === duration) {
            return true;
        }
    } 
}