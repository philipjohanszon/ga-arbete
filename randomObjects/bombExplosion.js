function BombExplosion(pos) {
    //VARIABLES
    this.position = pos;
    this.frameDuration = 50;
    this.frame = 0;
    this.colorDropOfMultiplier = 3;
    this.size = 50;

    //DRAWS THE BOMB
    this.draw = () => {
        //CREATES A NEW TEMPORARY STYLE
        push();
        //REMOVES STROKE
        noStroke();
        //CREATES A FILL WITH A FADING OPACITY TO MIMIC A EXPLOSION
        fill(239,142,41, 100/(this.frame/this.colorDropOfMultiplier));
        //DRAWS THE SHAPE OF THE EXPLOSION
        ellipse(this.position.x, this.position.y, this.size, this.size);
        pop();
        //ADDS ONE TO THE FRAME COUNTER
        this.frame++;
    }

    //IF THE EXPLOSION DURATION IS OVER THEN DELETE IT IN SKETCH
    this.checkForEnd = () => {
        if(this.frame === this.frameDuration) {
            return true;
        }
    }
}