#  Philip Johanssons GA ARBETE (TE3B)

# Nevada Daybreak

## Controls

#### W

Walk forward

#### S

Walk backwards

#### A

Walk to the left

#### D

Walk to the right

#### Space

Attack

#### Mouse

Use your mouse to aim

## How to start

#### Download the http-server NPM package

`npm install http-server -g`

This will install it globally so you can use it in any directory

#### Start the http-server

Open a terminal in the directory that the game is located in and write:

`http-server`

Then go to the URL: [http://127.0.0.1:8080](http://127.0.0.1:8080 "Nevada Daybreak")
