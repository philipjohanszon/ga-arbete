//SETUP FUNCTION WILL RUN AT START AND CREATE A CANVAS, SET THE FRAMERATE, SET THE COLORMODE, SET THE ANGLEMODE
function setup() {
  createCanvas(canvas.x, canvas.y);
  frameRate(60);
  colorMode(RGB, 255);
  angleMode(DEGREES);
  //CREATES A PLAYER
  player = new Player();
  //TURNS OFF THE CURSOR
  noCursor();
  //CREATES A NEW GAME OBJECT
  game = new Game();
  //STARTS THE GAME
  game.begin();
}

function draw() {
  //DRAWS THE BACKGROUND IMAGE UNDER THE UI
  image(backgroundIMG, 0, 200);

  //LOOPS THROUGH IT SINCE I NEED TO KNOW WHEN DROPS ARE PICKED UP
  if(game.notStarted) {
    if(droppedItems.length === 0) {
      game.notStarted = false;
      game.enemySpawnScore += 200;
      game.startNewRound();
    }
  }
  //CHECK IF SPAWNING ENEMIES AND CHECK FOR A NEW ROUND
  game.spawnEnemies();
  game.checkForNewRound();

  //DRAWS THE DROPPED ITEMS
  drawDroppedItems();

  //BOMB EXPLOSIONS ARE DRAWN UNDER CHARACTERS AND THROWABLES
  for(let i = 0; i < bombExplosions.length; i++) {
    bombExplosions[i].draw();
    //IF THE ANIMATION IS DONE THEN REMOVE IT
    if(bombExplosions[i].checkForEnd()) {
      bombExplosions.splice(i, 1);
    }
  }

  //DISPLAYS, MOVES AND ATTACKS AND UPDATES ATTACK COOLDOWNS FOR ALL ENEMIES
  for(let enemy in enemies) {
    for(let i = 0; i < enemies[enemy].length; i++) {
      enemies[enemy][i].draw();
      enemies[enemy][i].move();
      if(enemy === "bombers") {
        enemies[enemy][i].checkForAttack(i);
      }
      else {
        enemies[enemy][i].checkForAttack();
      }
      if(enemy != "bombers") {
        enemies[enemy][i].updateAttackCooldown();
      }
    }
  }

  //DRAWS PLAYER AND CHECKS FOR PICKINGUP OF ITEMS
  player.draw();
  player.pickUpItem();

  //ARROWS DISPLAY, MOVEMENT, DAMAGING ENEMIES AND ALSO SELF DESTRUCTING WHEN IT HAS HIT ITS TARGET
  for(let i = 0; i < arrowsInAir.length; i++) {
    arrowsInAir[i].draw();
    arrowsInAir[i].move();
    arrowsInAir[i].hitCharacter();
    if(arrowsInAir[i].selfDestruct()) {
      //REMOVES THE ARROW THAT HIT ITS TARGET
      arrowsInAir.splice(i, 1)
    }
  }

  //BOMBS DISPLAY, MOVEMENT, DAMAGING ENEMIES AND ALSO SELF DESTRUCTING WHEN IT HAS HIT ITS TARGET
  for(let i = 0; i < bombsInAir.length; i++) {
    bombsInAir[i].draw();
    bombsInAir[i].move();
    if(bombsInAir[i].checkIfLanded()) {
      bombsInAir.splice(i, 1);
    }
  }


  //DRAWS AND DELETES UI TEXT
  for(let i = 0; i < UITexts.length; i++) {
    UITexts[i].draw();
    if(UITexts[i].delete()) {
      UITexts.splice(i, 1);
    }
  }

  //DRAWS THE UI, DRAWS THE CURSOR AND CHECKS FOR KEYPRESSES
  drawUI();
  drawCursor();
  checkKeyPresses();
}

//CHECKS FOR KEYPRESSES
function checkKeyPresses() {
    
  //W
  if(keyIsDown(87)) {
      player.move({y: -1, x: 0});
  }
  
  //S
  if(keyIsDown(83)) {
      player.move({y: 1, x: 0});
  }

  //A
  if(keyIsDown(65)) {
      player.move({y: 0, x: -1})
  }

  //D
  if(keyIsDown(68)) {
      player.move({y: 0, x: 1});
  }

  //SPACE
  if(keyIsDown(32)) {
      player.useItem();
  }

  //1
  if(keyIsDown(49)) {
      player.switchWeapon(1);
  }

  //2
  if(keyIsDown(50)) {
      player.switchWeapon(2);
  }

  //3
  if(keyIsDown(51)) {
    player.switchWeapon(3);
  }

  //4
  if(keyIsDown(52)) {
    player.switchWeapon(4);
  }

}