function preload() {

    //UI
    font = loadFont("randomAssets/DOMINICA.TTF");

    cursorIMG = loadImage("randomAssets/cursor.png");
    topUIIMG = loadImage("background/topUI.png");
    backgroundIMG = loadImage("background/background.png");
    shieldUIIMG = loadImage("background/shieldUI.png");
    bombWithStrokeIMG = loadImage("randomAssets/bombWithStrokeIMG.png");

    //PLAYER
    playerNoWeaponIMG = loadImage("characters/player/sprites/playerNoWeapon.png");
    playerWithSwordIMG = loadImage("characters/player/sprites/playerWithSword.png");
    playerWithShieldIMG = loadImage("characters/player/sprites/playerWithShield.png");
    playerWithBowIMG = loadImage("characters/player/sprites/playerWithBow.png");
    playerWithBombIMG = loadImage("characters/player/sprites/playerWithBomb.png");
    
    //Player Sword Animations
    playerSwordAnim0 = loadImage("characters/player/sprites/playerSwordAnim0.png");
    playerSwordAnim1 = loadImage("characters/player/sprites/playerSwordAnim1.png");
    playerSwordAnim2 = loadImage("characters/player/sprites/playerSwordAnim2.png");
    playerSwordAnim3 = loadImage("characters/player/sprites/playerSwordAnim3.png");
    playerSwordAnim4 = loadImage("characters/player/sprites/playerSwordAnim4.png");
    playerSwordAnim5 = loadImage("characters/player/sprites/playerSwordAnim5.png");
    playerSwordAnim6 = loadImage("characters/player/sprites/playerSwordAnim6.png");
    playerSwordAnim7 = loadImage("characters/player/sprites/playerSwordAnim7.png");

    //Player Bow Animations
    playerBowAnim0 = loadImage("characters/player/sprites/playerBowAnim0.png");
    playerBowAnim1 = loadImage("characters/player/sprites/playerBowAnim1.png");
    playerBowAnim2 = loadImage("characters/player/sprites/playerBowAnim2.png");
    playerBowAnim3 = loadImage("characters/player/sprites/playerBowAnim3.png");
    playerBowAnim4 = loadImage("characters/player/sprites/playerBowAnim4.png");
    playerBowAnim5 = loadImage("characters/player/sprites/playerBowAnim5.png");
    playerBowAnim6 = loadImage("characters/player/sprites/playerBowAnim6.png");
    playerBowAnim7 = loadImage("characters/player/sprites/playerBowAnim7.png");

    //Player Bomb Animations
    playerBombAnim0 = loadImage("characters/player/sprites/playerBombAnim0.png");
    playerBombAnim1 = loadImage("characters/player/sprites/playerBombAnim1.png");
    playerBombAnim2 = loadImage("characters/player/sprites/playerBombAnim2.png");
    playerBombAnim3 = loadImage("characters/player/sprites/playerBombAnim3.png");
    playerBombAnim4 = loadImage("characters/player/sprites/playerBombAnim4.png");
    playerBombAnim5 = loadImage("characters/player/sprites/playerBombAnim5.png");
    playerBombAnim6 = loadImage("characters/player/sprites/playerBombAnim6.png");
    playerBombAnim7 = loadImage("characters/player/sprites/playerBombAnim7.png");

    //SWORDFIGHTER
    swordFighterStandardIMG = loadImage("characters/enemies/swordFighter/sprites/standard.png");

    //SWORDFIGHTER ANIMATION
    swordFighterAnim0 = loadImage("characters/enemies/swordFighter/sprites/attackAnim0.png");
    swordFighterAnim1 = loadImage("characters/enemies/swordFighter/sprites/attackAnim1.png");
    swordFighterAnim2 = loadImage("characters/enemies/swordFighter/sprites/attackAnim2.png");
    swordFighterAnim3 = loadImage("characters/enemies/swordFighter/sprites/attackAnim3.png");
    swordFighterAnim4 = loadImage("characters/enemies/swordFighter/sprites/attackAnim4.png");
    swordFighterAnim5 = loadImage("characters/enemies/swordFighter/sprites/attackAnim5.png");
    swordFighterAnim6 = loadImage("characters/enemies/swordFighter/sprites/attackAnim6.png");
    swordFighterAnim7 = loadImage("characters/enemies/swordFighter/sprites/attackAnim7.png");

    //ARCHER
    archerStandardIMG = loadImage("characters/enemies/archer/sprites/standard.png");

    //ARCHER ANIMATION
    archerAnim0 = loadImage("characters/enemies/archer/sprites/attackAnim0.png");
    archerAnim1 = loadImage("characters/enemies/archer/sprites/attackAnim1.png");
    archerAnim2 = loadImage("characters/enemies/archer/sprites/attackAnim2.png");
    archerAnim3 = loadImage("characters/enemies/archer/sprites/attackAnim3.png");
    archerAnim4 = loadImage("characters/enemies/archer/sprites/attackAnim4.png");
    archerAnim5 = loadImage("characters/enemies/archer/sprites/attackAnim5.png");
    archerAnim6 = loadImage("characters/enemies/archer/sprites/attackAnim6.png");
    archerAnim7 = loadImage("characters/enemies/archer/sprites/attackAnim7.png");

    //BOMBER
    bomberStandardIMG = loadImage("characters/enemies/bomber/sprites/standard.png");

    swordIMG = loadImage("randomAssets/sword.png");
    shieldIMG = loadImage("randomAssets/shield.png");
    bowIMG = loadImage("randomAssets/bow.png");
    bombIMG = loadImage("randomAssets/bomb.png");
}