const canvas = {
    x: 800,
    y: 1000
  }

  let game;
  let player;
  
  let cursor = {
    length: 20,
    height: 5,
    borderRadius: 3
  };
  
  let font;

  // ---------------- SEMI IN GAME VARIABLES -------

  let droppedItemSize = 30;
  let bombDropChance = 20;
  let amountOfArrowsInDrop = 5;

  // -----------------------------------------------
  
  // ---------------- IN GAME VARIABLES ------------

  let itemSword;
  let itemShield;
  let itemBow;

  let itemArrows;
  let itemBomb;

  let enemies = {swordFighters: [], archers: [], bombers: []};
  let droppedItems = [];
  let arrowsInAir = [];
  let bombsInAir = [];
  let bombExplosions = [];
  let UITexts = [];

  // -----------------------------------------------


  // ---------------- COLLIDERS --------------------
  
  let ravines = 
  [
    {x: 70, y: 270, w: 80, h: 380},
    {x: 65, y: 780, w: 80, h: 180},
    {x: 580, y: 550, w: 220, h: 100}
  ]
  
  // -----------------------------------------------
  
  // ---------------- IMAGES -----------------------
  
  
  //UI?
  let cursorIMG;

  let topUIIMG;
  let backgroundIMG;
  let shieldUIIMG;
  let bombWithStrokeIMG;
  
  //PLAYER
  let playerNoWeaponIMG;
  let playerWithSwordIMG;
  let playerWithShieldIMG;
  let playerWithBowIMG;
  let playerWithBombIMG;
  

  //Player animations
  let playerSwordAnim0;
  let playerSwordAnim1;
  let playerSwordAnim2;
  let playerSwordAnim3;
  let playerSwordAnim4;
  let playerSwordAnim5;
  let playerSwordAnim6;
  let playerSwordAnim7;

  let playerBowAnim0;
  let playerBowAnim1;
  let playerBowAnim2;
  let playerBowAnim3;
  let playerBowAnim4;
  let playerBowAnim5;
  let playerBowAnim6;
  let playerBowAnim7;

  let playerBombAnim0;
  let playerBombAnim1;
  let playerBombAnim2;
  let playerBombAnim3;
  let playerBombAnim4;
  let playerBombAnim5;
  let playerBombAnim6;
  let playerBombAnim7;

  //SWORDFIGHTER
  let swordFighterStandardIMG;

  //SWORDFIGHTER ANIMATION
  let swordFighterAnim0;
  let swordFighterAnim1;
  let swordFighterAnim2;
  let swordFighterAnim3;
  let swordFighterAnim4;
  let swordFighterAnim5;
  let swordFighterAnim6;
  let swordFighterAnim7;

  //ARCHER
  let archerStandardIMG;

  //ARCHER ANIMATION
  let archerAnim0;
  let archerAnim1;
  let archerAnim2;
  let archerAnim3;
  let archerAnim4;
  let archerAnim5;
  let archerAnim6;
  let archerAnim7;

  //BOMBER
  let bomberStandardIMG;

  //ITEMS
  let swordIMG;
  let shieldIMG;
  let bowIMG;
  let bombIMG;

  // -----------------------------------------------
  